Response to the "Conference Track Management" code Challenge
============================================================

this Repo contains my response to the code challenge described at [ConferenceTrackManagement.md](ConferenceTrackManagement.md)

## Setting up
* Verify that you are running at least node 6.9.x and npm 3.x.x by running node -v and npm -v in a terminal/console window

* This code was developed and tested with Google Chrome v 62.0 (64 bits). Because of that, in order to run the test cases, it requires Google Chrome installed.

execute the following to install angular CLI (ng command)
```
npm install -g @angular/cli@1.6.6
```

execute the following to install dependencies locally
```
npm install 
```

## Serving the application

Execute
```
ng serve -o
```

Navigate to `http://localhost:4200/` (if necessary) 

Please make sure to have the browser console window open for added error reporting

## Running unit tests

execute the following to run unit tests
```
ng test
```

## Code coverage report

To generate the coverage report execute
```
ng test --code-coverage
```

The coverage report can be accessed from `.\coverage\index.html`
