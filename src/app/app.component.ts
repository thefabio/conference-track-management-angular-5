import { Component } from '@angular/core';
import {Talk} from './load-data/talk';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Conference Track Management';
  allTalks: Talk[];
  CreateConferenceTracks(newTalks): void {
    this.allTalks = newTalks;
  }
}
