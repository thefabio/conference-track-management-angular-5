import { Injectable } from '@angular/core';
import {Talk} from './talk';
import {TrackScheduleService} from '../track-schedule/track-schedule.service';

@Injectable()
export class LoadDataService {

  constructor(private trackScheduleService: TrackScheduleService) { }
  private CleanUpRawDataNewLinesAndSplit(rawInputData: string): string[] {
    const cleanRawData = rawInputData.replace(/\r/g, '');
    const linesArray = cleanRawData.split('\n');
    if (linesArray.length && linesArray[linesArray.length - 1].replace(/ /g, '').length === 0) {
      // if the last line of the array is only spaces, remove it
      linesArray.pop();
    }
    return linesArray;
  }
  ValidateInputData(rawInputData: string): string[] {
    const inputDataLines = this.CleanUpRawDataNewLinesAndSplit(rawInputData);
    const errorsFound = [];
    for (let i = 0; i < inputDataLines.length; i++) {
      if (inputDataLines[i].replace(/ /g, '').length === 0) {
        errorsFound.push('Empty Line at ' + (i + 1) );
        continue;
      }

      const lineParts = inputDataLines[i].split(' ');

      if (lineParts.length < 2) {
        errorsFound.push('Invalid Line at ' + (i + 1) );
        continue;
      }

      const durationString = lineParts.pop();
      const title = lineParts.join(' ');

      if (/\d/.test(title)) {
        errorsFound.push('Title has numbers, line ' + (i + 1) );
        continue;
      }

      if (durationString !== 'lightning') {
        if (!/min/i.test(durationString)) {
          errorsFound.push('E1 - Invalid Duration "' + durationString + '", line ' + (i + 1) );
          continue;
        }
        if (!/\d/.test(durationString)) {
          errorsFound.push('E2 - Invalid Duration "' + durationString + '", line ' + (i + 1) );
          continue;
        }
        const duration = Number(durationString.replace(/[^0-9]/g, ''));
        if (duration > this.trackScheduleService.MorningTalksDuration ||
          duration > this.trackScheduleService.AfternoonTalksDuration) {
          errorsFound.push('Invalid Duration "' + duration + '" too long, line ' + (i + 1) );
          continue;
        }
      }
    }
    return errorsFound;
  }

  ParseInputData(rawInputData: string): Talk[] {
    if (this.ValidateInputData(rawInputData).length) {
      return [];
    }

    const talks: Talk[] = [];
    const inputDataLines = this.CleanUpRawDataNewLinesAndSplit(rawInputData);
    for (let i = 0; i < inputDataLines.length; i++) {
      const lineParts = inputDataLines[i].split(' ');
      const durationString = lineParts.pop();
      talks.push({
        Duration: durationString === 'lightning' ? 5 : Number(durationString.replace(/[^0-9]/g, '')),
        Title: inputDataLines[i]
      });
    }

    return talks;
  }
}
