import { TestBed, inject } from '@angular/core/testing';

import { LoadDataService } from './load-data.service';
import {TrackScheduleService} from '../track-schedule/track-schedule.service';

describe('LoadDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoadDataService, TrackScheduleService]
    });
  });

  it('should be created', inject([LoadDataService], (service: LoadDataService) => {
    expect(service).toBeTruthy();
  }));

  describe('ValidateInputData', () => {
    it('Validates successfully when title is provided ' +
      'and duration is provided in minutes', inject([LoadDataService], (service: LoadDataService) => {
      const twoTalks = 'Ruby Errors from Mismatched Gem Versions 45min\n' +
        'Ruby Errors from Mismatched Gem Versions 45min';
      const responseErrors = service.ValidateInputData(twoTalks);
      expect(responseErrors.length).toBe(0);
    }));

    it('Validates successfully when title is provided ' +
      'and duration is provided as "lightning"', inject([LoadDataService], (service: LoadDataService) => {
      const twoTalks = 'Ruby Errors from Mismatched Gem Versions lightning\r\n' +
        'Ruby Errors from Mismatched Gem Versions lightning';
      const responseErrors = service.ValidateInputData(twoTalks);
      expect(responseErrors.length).toBe(0);
    }));

    it('Validates with error when empty line is provided on the middle of raw input',
      inject([LoadDataService], (service: LoadDataService) => {
        const twoTalks = 'Ruby Errors from Mismatched Gem Versions 45min\n' +
            '\n' +
          'Ruby Errors from Mismatched Gem Versions 45min';
        const responseErrors = service.ValidateInputData(twoTalks);
        expect(responseErrors.length).toBe(1);
    }));

    it('Validates with error when line with empty spaces is provided',
      inject([LoadDataService], (service: LoadDataService) => {
        const twoTalks = 'Ruby Errors from Mismatched Gem Versions 45min\r\n' +
          '    \r\n' +
          'Ruby Errors from Mismatched Gem Versions 45min';
        const responseErrors = service.ValidateInputData(twoTalks);
        expect(responseErrors.length).toBe(1);
      }));
    it('Validates successfully when line with empty spaces is provided at the end of input',
      inject([LoadDataService], (service: LoadDataService) => {
        const twoTalks = 'Ruby Errors from Mismatched Gem Versions 45min\r\n' +
          '    ';
        const responseErrors = service.ValidateInputData(twoTalks);
        expect(responseErrors.length).toBe(0);
      }));

    it('Validates with error when talk title has numbers in it',
      inject([LoadDataService], (service: LoadDataService) => {
        const twoTalks = 'Ruby Error 1 from Mismatched Gem Versions 45min\r\n' +
          'Ruby Errors from Gems 60min';
        const responseErrors = service.ValidateInputData(twoTalks);
        expect(responseErrors.length).toBe(1);
      }));

    it('Validates with error when no duration is provided',
      inject([LoadDataService], (service: LoadDataService) => {
        const twoTalks = 'Ruby Error from Mismatched Gem Versions\r\n' +
          'Ruby Errors from Gems 60min';
        const responseErrors = service.ValidateInputData(twoTalks);
        expect(responseErrors.length).toBe(1);
      }));
    it('Validates with error when talk duration is greater then the duration of morning or afternoon sessions',
      inject([LoadDataService], (service: LoadDataService) => {
        const twoTalks = 'Ruby Error from Mismatched Gem Versions 45min\r\n' +
          'Ruby Errors from Gems 360min';
        const responseErrors = service.ValidateInputData(twoTalks);
        expect(responseErrors.length).toBe(1);
      }));
  });

  describe('ParseInputData', () => {
    it('Parses title and duration when duration is provided in minutes',
      inject([LoadDataService], (service: LoadDataService) => {
        const twoTalks = 'Ruby Errors from Mismatched Gem Versions 45min\n' +
          'Ruby Errors from Gems 60min';
        const talks = service.ParseInputData(twoTalks);
        expect(talks.length).toBe(2);
        expect(talks[0].Title).toBe('Ruby Errors from Mismatched Gem Versions 45min');
        expect(talks[0].Duration).toBe(45);
        expect(talks[1].Title).toBe('Ruby Errors from Gems 60min');
        expect(talks[1].Duration).toBe(60);
      }));
    it('Parses title and duration when duration is provided as "lightning"',
      inject([LoadDataService], (service: LoadDataService) => {
        const twoTalks = 'Ruby Errors from Mismatched Gem Versions lightning\r\n' +
          'Ruby Errors from Gems lightning';
        const talks = service.ParseInputData(twoTalks);
        expect(talks.length).toBe(2);
        expect(talks[0].Title).toBe('Ruby Errors from Mismatched Gem Versions lightning');
        expect(talks[0].Duration).toBe(5);
        expect(talks[1].Title).toBe('Ruby Errors from Gems lightning');
        expect(talks[1].Duration).toBe(5);
      }));
  });
});
