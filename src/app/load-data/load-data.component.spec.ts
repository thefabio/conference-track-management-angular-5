import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadDataComponent } from './load-data.component';
import {LoadDataService} from './load-data.service';
import {FormsModule} from '@angular/forms';
import {TrackScheduleService} from '../track-schedule/track-schedule.service';

describe('LoadDataComponent', () => {
  let component: LoadDataComponent;
  let fixture: ComponentFixture<LoadDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadDataComponent ],
      imports: [ FormsModule ],
      providers: [ LoadDataService, TrackScheduleService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
