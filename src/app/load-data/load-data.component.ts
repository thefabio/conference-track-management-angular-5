import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Talk} from './talk';
import {LoadDataService} from './load-data.service';

@Component({
  selector: 'app-load-data',
  templateUrl: './load-data.component.html',
  styleUrls: ['./load-data.component.css']
})
export class LoadDataComponent implements OnInit {
  debugMode = false;
  public inputDataText: string;
  public talks: Talk[];
  public inputErrors: string[];

  @Output() inputRead: EventEmitter<Talk[]> = new EventEmitter();

  constructor(private loadDataService: LoadDataService) { }

  btnLoadContentsClick(): void {
    document.getElementById('fileInput').click();
  }
  OnFileRead(event: any): void {

    if (!event.srcElement.files || !event.srcElement.files.length) {
      return;
    }
    const a = event.srcElement.files[0];

    const reader = new FileReader();
    reader.onload = () => {
      this.setInputDataText(reader.result);
    };
    reader.readAsText(a);
  }
  setInputDataText(newValue: string) {
    this.inputDataText = newValue;
  }
  ngOnInit() {
    this.inputDataText = 'Writing Fast Tests Against Enterprise Rails 60min\r\n' +
    'Overdoing it in Python 45min\r\n' +
    'Lua for the Masses 30min\r\n' +
    'Ruby Errors from Mismatched Gem Versions 45min\r\n' +
    'Common Ruby Errors 45min\r\n' +
    'Rails for Python Developers lightning\r\n' +
    'Communicating Over Distance 60min\r\n' +
    'Accounting-Driven Development 45min\r\n' +
    'Woah 30min\r\n' +
    'Sit Down and Write 30min\r\n' +
    'Pair Programming vs Noise 45min\r\n' +
    'Rails Magic 60min\r\n' +
    'Ruby on Rails: Why We Should Move On 60min\r\n' +
    'Clojure Ate Scala (on my project) 45min\r\n' +
    'Programming in the Boondocks of Seattle 30min\r\n' +
    'Ruby vs. Clojure for Back-End Development 30min\r\n' +
    'Ruby on Rails Legacy App Maintenance 60min\r\n' +
    'A World Without HackerNews 30min\r\n' +
    'User Interface CSS in Rails Apps 30min';
  }
  ParseData(): void {
    this.inputErrors = this.loadDataService.ValidateInputData(this.inputDataText);

    if (!this.inputErrors.length) {
      this.talks = this.loadDataService.ParseInputData(this.inputDataText);
      this.inputRead.emit(null);
    } else {
      this.talks = [];
    }

    this.inputRead.emit(this.talks);
  }
}
