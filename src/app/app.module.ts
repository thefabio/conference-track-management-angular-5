import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { LoadDataComponent } from './load-data/load-data.component';
import {FormsModule} from '@angular/forms';
import {LoadDataService} from './load-data/load-data.service';
import { TrackScheduleComponent } from './track-schedule/track-schedule.component';
import {TrackScheduleService} from './track-schedule/track-schedule.service';


@NgModule({
  declarations: [
    AppComponent,
    LoadDataComponent,
    TrackScheduleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    LoadDataService,
    TrackScheduleService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
