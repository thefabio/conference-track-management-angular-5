import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {LoadDataComponent} from './load-data/load-data.component';
import {FormsModule} from '@angular/forms';
import {LoadDataService} from './load-data/load-data.service';
import {TrackScheduleComponent} from './track-schedule/track-schedule.component';
import {TrackScheduleService} from './track-schedule/track-schedule.service';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppComponent, LoadDataComponent, TrackScheduleComponent ],
      imports: [ FormsModule ],
      providers: [ LoadDataService, TrackScheduleService ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'Conference Track Management'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Conference Track Management');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to the Conference Track Management app!');
  }));
});
