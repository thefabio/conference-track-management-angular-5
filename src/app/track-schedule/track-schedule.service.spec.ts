import { TestBed, inject } from '@angular/core/testing';

import { TrackScheduleService } from './track-schedule.service';
import {Track} from './track';
import {Talk} from '../load-data/talk';

describe('TrackScheduleService', () => {
  let track1: Track;
  let track2: Track;
  let talks: Talk[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrackScheduleService]
    });
    track1 = {
      Sessions: [
        {
          Talk: {
            Duration: 30,
            Title: 'Talk A'
          },
          StartTime: '09:00AM'
        },
        {
          Talk: {
            Duration: 10,
            Title: 'Talk B'
          },
          StartTime: '09:30AM'
        },
        {
          Talk: {
            Duration: 20,
            Title: 'Talk C'
          },
          StartTime: '09:40AM'
        }
      ]
    };
    track2 = {
      Sessions: [
        {
          Talk: {
            Duration: 60,
            Title: 'Talk A'
          },
          StartTime: '09:00AM'
        },
        {
          Talk: {
            Duration: 60,
            Title: 'Talk B'
          },
          StartTime: '10:00AM'
        },
        {
          Talk: {
            Duration: 60,
            Title: 'Talk C'
          },
          StartTime: '11:00AM'
        }
      ]
    };
    talks = [
      {
        Title: 'Talk J',
        Duration: 45
      },
      {
        Title: 'Talk K',
        Duration: 45
      },
      {
        Title: 'Talk L',
        Duration: 5
      },
      {
        Title: 'Talk M',
        Duration: 60
      },
      {
        Title: 'Talk N',
        Duration: 30
      },
      {
        Title: 'Talk O',
        Duration: 60
      },
      {
        Title: 'Talk P',
        Duration: 60
      },
      {
        Title: 'Talk Q',
        Duration: 60
      },
      {
        Title: 'Talk R',
        Duration: 60
      }
    ];
  });

  it('should be created', inject([TrackScheduleService], (service: TrackScheduleService) => {
    expect(service).toBeTruthy();
  }));

  it('Calculates the Duration of talks in a track', inject([TrackScheduleService], (service: TrackScheduleService) => {
    expect(service.GetTrackLength(track1)).toBe(60);
    expect(service.GetTrackLength(track2)).toBe(180);
  }));

  it('Calculates the Duration of an array of talks', inject([TrackScheduleService], (service: TrackScheduleService) => {
    expect(service.GetTalksLength(track1.Sessions.map(x => x.Talk))).toBe(60);
    expect(service.GetTalksLength(track2.Sessions.map(x => x.Talk))).toBe(180);
  }));

  it('Calculates the next session item on a Track when the track and the next talk are provided',
    inject([TrackScheduleService], (service: TrackScheduleService) => {
    const nextTalk: Talk = {
      Title: 'The Talk',
      Duration: 45
    };
    const nextSession = service.GetNextSessionDetailsForTalk(track1, nextTalk);

    expect(nextSession.StartTime).toBe('10:00AM');
    expect(nextSession.Talk.Title).toBe(nextTalk.Title);
    expect(nextSession.Talk.Duration).toBe(nextTalk.Duration);
  }));

  it('Creates the Lunch Break on a Track', inject([TrackScheduleService], (service: TrackScheduleService) => {
      service.FillInLunch(track2);
      const lunchSession = track2.Sessions[track2.Sessions.length - 1];

      expect(lunchSession.StartTime).toBe('12:00PM');
      expect(lunchSession.Talk.Title).toBe('Lunch');
      expect(lunchSession.Talk.Duration).toBe(60);
    }));

  it('Creates Lunch Break on a Track with few morning sessions', inject([TrackScheduleService], (service: TrackScheduleService) => {
    service.FillInLunch(track1);
    const lunchSession = track1.Sessions[track1.Sessions.length - 1];

    expect(lunchSession.StartTime).toBe('12:00PM');
    expect(lunchSession.Talk.Title).toBe('Lunch');
    expect(lunchSession.Talk.Duration).toBe(180);
  }));

  it('Creates Networking Even on a Track', inject([TrackScheduleService], (service: TrackScheduleService) => {
    service.FillInNetworkingEvent(track1);
    const lunchSession = track1.Sessions[track1.Sessions.length - 1];

    expect(lunchSession.StartTime).toBe('04:00PM');
    expect(lunchSession.Talk.Title).toBe('Networking Event');
    expect(lunchSession.Talk.Duration).toBe(0);
  }));

  it('Creates Networking Even on a Track', inject([TrackScheduleService], (service: TrackScheduleService) => {
    track2.Sessions.push({
      Talk: {
        Duration: 300,
        Title: 'Talk X'
      },
      StartTime: '04:00AM'
    });
    service.FillInNetworkingEvent(track2);
    const lunchSession = track2.Sessions[track2.Sessions.length - 1];

    expect(lunchSession.StartTime).toBe('05:00PM');
    expect(lunchSession.Talk.Title).toBe('Networking Event');
    expect(lunchSession.Talk.Duration).toBe(0);
  }));

  it('Loads talks into a Track giving talks and a duration', inject([TrackScheduleService], (service: TrackScheduleService) => {
    service.FillInTalks(track1, talks, 10);
    const addedSession = track1.Sessions[track1.Sessions.length - 1];

    expect(addedSession.StartTime).toBe('10:00AM');
    expect(addedSession.Talk.Title).toBe('Talk L');
    expect(addedSession.Talk.Duration).toBe(5);
  }));

  it('Loads talks into a Track giving talks and a duration', inject([TrackScheduleService], (service: TrackScheduleService) => {
    service.FillInTalks(track1, talks, 130);
    const addedSession1 = track1.Sessions[track1.Sessions.length - 3];
    const addedSession2 = track1.Sessions[track1.Sessions.length - 2];
    const addedSession3 = track1.Sessions[track1.Sessions.length - 1];

    expect(addedSession1.StartTime).toBe('10:00AM');
    expect(addedSession1.Talk.Title).toBe('Talk M');
    expect(addedSession2.StartTime).toBe('11:00AM');
    expect(addedSession2.Talk.Title).toBe('Talk O');
    expect(addedSession3.StartTime).toBe('12:00PM');
    expect(addedSession3.Talk.Title).toBe('Talk L');
  }));

  it('Calculates Tracks for talks (Main Example)', inject([TrackScheduleService], (service: TrackScheduleService) => {
    const exapleTalks: Talk[] = [
      {Duration: 60, Title: 'Writing Fast Tests Against Enterprise Rails 60min'},
      {Duration: 45, Title: 'Overdoing it in Python 45min'},
      {Duration: 30, Title: 'Lua for the Masses 30min'},
      {Duration: 45, Title: 'Ruby Errors from Mismatched Gem Versions 45min'},
      {Duration: 45, Title: 'Common Ruby Errors 45min'},
      {Duration: 5, Title: 'Rails for Python Developers lightning'},
      {Duration: 60, Title: 'Communicating Over Distance 60min'},
      {Duration: 45, Title: 'Accounting-Driven Development 45min'},
      {Duration: 30, Title: 'Woah 30min'},
      {Duration: 30, Title: 'Sit Down and Write 30min'},
      {Duration: 45, Title: 'Pair Programming vs Noise 45min'},
      {Duration: 60, Title: 'Rails Magic 60min'},
      {Duration: 60, Title: 'Ruby on Rails: Why We Should Move On 60min'},
      {Duration: 45, Title: 'Clojure Ate Scala (on my project) 45min'},
      {Duration: 30, Title: 'Programming in the Boondocks of Seattle 30min'},
      {Duration: 30, Title: 'Ruby vs. Clojure for Back-End Development 30min'},
      {Duration: 60, Title: 'Ruby on Rails Legacy App Maintenance 60min'},
      {Duration: 30, Title: 'A World Without HackerNews 30min'},
      {Duration: 30, Title: 'User Interface CSS in Rails Apps 30min'},
    ];
    const tracks = service.GetTracksFromTalks(exapleTalks);
    expect(tracks.length).toBe(2);

    expect(tracks[0].Sessions[0].StartTime).toBe('09:00AM');
    expect(tracks[0].Sessions[3].Talk.Duration).toBe(45);
    expect(tracks[0].Sessions[3].StartTime).toBe('11:15AM');
    expect(tracks[0].Sessions[4].Talk.Title).toBe('Lunch');
    expect(tracks[0].Sessions[9].Talk.Duration).toBe(60);
    expect(tracks[0].Sessions[9].StartTime).toBe('04:00PM');
    expect(tracks[0].Sessions[10].Talk.Title).toBe('Networking Event');
    expect(tracks[0].Sessions[10].StartTime).toBe('05:00PM');

    expect(tracks[1].Sessions[0].StartTime).toBe('09:00AM');
    expect(tracks[1].Sessions[3].Talk.Duration).toBe(30);
    expect(tracks[1].Sessions[3].StartTime).toBe('11:30AM');
    expect(tracks[1].Sessions[4].Talk.Title).toBe('Lunch');
    expect(tracks[1].Sessions[10].Talk.Duration).toBe(30);
    expect(tracks[1].Sessions[10].StartTime).toBe('03:35PM');
    expect(tracks[1].Sessions[11].Talk.Title).toBe('Networking Event');
    expect(tracks[1].Sessions[11].StartTime).toBe('04:05PM');
  }));
});
