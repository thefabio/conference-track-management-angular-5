import { Injectable } from '@angular/core';
import {Talk} from '../load-data/talk';
import {Track} from './track';
import {TrackSession} from './track-session';
import moment = require('moment');

@Injectable()
export class TrackScheduleService {
  private MorningStartTime = '09:00AM';
  private _morningTalksDuration = 180; // until 12:00
  private LunchMinLength = 60; // until 13:00
  private _afternoonTalksDuration = 240; // until 17:00
  private MinTalksLengthForNetworkingEvent = 420; // from 16:00
  constructor() { }

  get MorningTalksDuration(): number {
    return this._morningTalksDuration;
  }
  get AfternoonTalksDuration(): number {
    return this._afternoonTalksDuration;
  }
  GetTalksLength(talks: Talk[]): number {
    return talks.reduce((acc, a) => acc + a.Duration, 0);
  }
  GetTrackLength(track: Track): number {
    return track.Sessions.reduce((acc, a) => acc + a.Talk.Duration, 0);
  }
  GetNextSessionDetailsForTalk(track: Track, talk: Talk): TrackSession {
    const initialTime = moment('20180101 ' + this.MorningStartTime, 'YYYYMMDD hh:mmA');
    const newSessionTime = initialTime.add(this.GetTrackLength(track), 'm').format('hh:mmA');

    return {
      StartTime: newSessionTime,
      Talk: talk
    };
  }
  FillInLunch(track: Track): void {
    const lunchSession = new TrackSession();
    lunchSession.StartTime = '12:00PM';
    lunchSession.Talk = {
      Duration: this.MorningTalksDuration + this.LunchMinLength - this.GetTrackLength(track),
      Title: 'Lunch'
    };
    track.Sessions.push(lunchSession);
  }
  FillInNetworkingEvent(track: Track): void {
    const networkEventTalk = {
      Duration: 0,
      Title: 'Networking Event'
    };
    const networkingEventSession = this.GetNextSessionDetailsForTalk(track, networkEventTalk);
    if (this.GetTrackLength(track) < this.MinTalksLengthForNetworkingEvent) {
      networkingEventSession.StartTime = '04:00PM';
    }
    track.Sessions.push(networkingEventSession);
  }
  FillInTalks(track: Track, remainingTalks: Talk[], targetLength: number): void {
    if (!remainingTalks.length) {
      return;
    }

    const usedTalks = [];
    const talksReProcess = [];
    for (let i = 0; i < remainingTalks.length; i++) {
      const refTalk = remainingTalks[i];
      if (usedTalks.indexOf(refTalk) >= 0 ) {
        continue;
      }
      const newLength = this.GetTalksLength(usedTalks);
      if (newLength + refTalk.Duration === targetLength) {
        usedTalks.push(refTalk);
        break;
      }

      if (newLength + refTalk.Duration < targetLength) {
        usedTalks.push(refTalk);
      } else {
        const usedTalksLessThenRefTalk = usedTalks.filter(y => y.Duration < refTalk.Duration).reverse();
        let acc = 0;
        const accTalks = [];
        for (let j = 0; j < usedTalksLessThenRefTalk.length; j++) {
          if (acc + usedTalksLessThenRefTalk[j].Duration < refTalk.Duration) {
            acc += usedTalksLessThenRefTalk[j].Duration;
            accTalks.push(usedTalksLessThenRefTalk[j]);
          }

          if (newLength + refTalk.Duration - acc === targetLength) {
            accTalks.forEach(x => usedTalks.splice(usedTalks.indexOf(x), 1));
            usedTalks.push(refTalk);
            break;
          }
        }
        if (acc && (newLength + refTalk.Duration - acc < targetLength)) {
          accTalks.forEach(x => {
            usedTalks.splice(usedTalks.indexOf(x), 1);
            talksReProcess.push(x);
          });
          usedTalks.push(refTalk);
        }
      }
    }

    usedTalks.forEach(x => {
      remainingTalks.splice(remainingTalks.indexOf(x), 1);
      track.Sessions.push(this.GetNextSessionDetailsForTalk(track, x));
    });
    if (talksReProcess.length) {
      this.FillInTalks(track, remainingTalks, targetLength - this.GetTalksLength(usedTalks));
    }
  }

  GetTracksFromTalks(talks: Talk[]): Track[] {
    // create a copy of the array and reverse it so the pop function can be used
    const remainingTalks: Talk[] = talks.map(x => Object.assign({}, x));
    const resultingTracks: Track[] = [];
    while (remainingTalks.length > 0) {
      const track = new Track();
      this.FillInTalks(track, remainingTalks, this.MorningTalksDuration);
      this.FillInLunch(track);
      this.FillInTalks(track, remainingTalks, this.AfternoonTalksDuration);
      this.FillInNetworkingEvent(track);
      resultingTracks.push(track);
   }
   return resultingTracks;
  }
}
