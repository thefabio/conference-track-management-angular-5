import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {TrackScheduleService} from './track-schedule.service';
import {Track} from './track';

@Component({
  selector: 'app-track-schedule',
  templateUrl: './track-schedule.component.html',
  styleUrls: ['./track-schedule.component.css']
})
export class TrackScheduleComponent implements OnInit, OnChanges {
  calculatedTracks: Track[];
  @Input() InputTalks;
  constructor(private trackScheduleService: TrackScheduleService) { }

  ngOnInit() {
    this.RefreshCalculatedTracks(this.InputTalks);
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.InputTalks) {
      this.RefreshCalculatedTracks(changes.InputTalks.currentValue);
    }
  }
  RefreshCalculatedTracks(newTalks): void {
    if (!newTalks || !newTalks.length) {
      this.calculatedTracks = null;
      return;
    }
    this.calculatedTracks = this.trackScheduleService.GetTracksFromTalks(newTalks);
  }
}
