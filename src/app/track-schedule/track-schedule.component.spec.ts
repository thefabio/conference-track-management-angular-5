import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackScheduleComponent } from './track-schedule.component';
import {FormsModule} from '@angular/forms';
import {TrackScheduleService} from './track-schedule.service';

describe('TrackScheduleComponent', () => {
  let component: TrackScheduleComponent;
  let fixture: ComponentFixture<TrackScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackScheduleComponent ],
      imports: [ FormsModule ],
      providers: [ TrackScheduleService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
